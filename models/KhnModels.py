from sqlalchemy import Column, Integer, String, TEXT, BOOLEAN
from ..common.Utils import get_current_time
from ..common.KhnBase import KHN_Object
from ..extensions import db


class Sys_Menu(KHN_Object, db.Model):
    __tablename__ = 'sys_menu'
    id = Column(Integer, primary_key=True)
    parent_id = Column(Integer, default=0, nullable=False)
    title = Column(String(255))
    onclick = Column(String(255),default="")
    description = Column(TEXT)
    status = Column(BOOLEAN, default=True)
    position = Column(Integer, default=100)
    access_level = Column(String(20), default="admin")

    created_date = db.Column(db.DateTime, nullable=False, default=get_current_time)
    created_by = Column(String(32))

    def load_childs(self):
        self.childs = Sys_Menu.query.filter_by(parent_id=self.id).order_by(Sys_Menu.position.asc(), Sys_Menu.title.asc()).all()

    @staticmethod
    def get_menu():
        menu = Sys_Menu.query.filter_by(parent_id=0).order_by(Sys_Menu.position.asc(), Sys_Menu.title.asc()).all()
        for m in menu:
            m.load_childs()

        new_menu = []
        for m in menu:
            item = m.json()
            item['subitems'] = [i.json() for i in m.childs]
            new_menu.append(item)

        return new_menu


class Sys_Parameter(KHN_Object, db.Model):
    __tablename__ = 'sys_parameter'
    id = Column(Integer, primary_key=True)
    name = Column(String(255), unique=True)
    value = Column(String(255),default="")
    description = Column(TEXT)
    created_date = db.Column(db.DateTime, nullable=False, default=get_current_time)
    created_by = Column(String(32))


class Sys_Usr_Actions(KHN_Object, db.Model):
    __tablename__ = 'sys_user_actions'
    id = Column(Integer, primary_key=True)
    id_user = Column(Integer, nullable=False)
    action = Column(String(32))
    url = Column(String(255))
    params = db.Column(db.TEXT)
    created_date = db.Column(db.DateTime, nullable=False, default=get_current_time)
    created_by = Column(String(32))
