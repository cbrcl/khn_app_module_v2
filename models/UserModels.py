from werkzeug import generate_password_hash, check_password_hash
from sqlalchemy import Column, ForeignKey, Integer, String, Date, DECIMAL, TEXT, BOOLEAN, desc, asc

from flask_login import UserMixin
from ..common import *
from ..extensions import db
from KhnModels import Sys_Usr_Actions

class User(KHN_Object, db.Model, UserMixin):
    __tablename__ = "sys_user"

    def __repr__(self):
        return '<User %r>' % (self.username)

    id = db.Column(db.Integer, primary_key=True)
    _username = db.Column('username', String(20), index=True, unique=True, nullable=False)
    first_name = db.Column(String(64), nullable=False)
    last_name = db.Column(String(64), nullable=False)
    email = db.Column(String(64), index=True)
    status = db.Column(Integer, default=1)
    created_on = db.Column(db.DateTime, nullable=False, default=get_current_time)

    # User Password
    _password = db.Column('password', String(255), nullable=False)

    def _get_username(self):
        return self._username

    def _set_username(self, username):
        self._username = username.lower()

    username = db.synonym('_username',
                          descriptor=property(_get_username,
                                              _set_username))

    def _get_password(self):
        return self._password

    def _set_password(self, password):
        self._password = generate_password_hash(password)

    password = db.synonym('_password',
                          descriptor=property(_get_password,
                                              _set_password))

    def log_action(self, action_name, params={}):
        action = Sys_Usr_Actions()
        action.id_user = self.id
        action.action = action_name.lower()
        action.url = "/%s" % request.url.replace(request.host_url, "")
        action.created_by = self.username

        action.params = json.dumps(params, default=json_serial)
        #action.params = jsonify(params)
        db.session.add(action)
        db.session.commit()

    def check_password(self, password):
        if self.password is None:
            return False
        return check_password_hash(self.password, password)

    # methods
    @classmethod
    def authenticate(cls, username, password):
        user = User.query.filter(db.or_(User.username == username)).first()

        if user:
            authenticated = user.check_password(password)
        else:
            authenticated = False
        return user, authenticated

    @classmethod
    def is_username_taken(cls, username):
        return db.session.query(db.exists().where(User.username == username)).scalar()

    @classmethod
    def is_email_taken(cls, email_address):
        return db.session.query(db.exists().where(User.email == email_address)).scalar()