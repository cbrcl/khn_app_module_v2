import sys
import setup
import os
sys.path.append(os.environ['PATH_APP'])
from src.app import create_app
from src.khn.extensions import db
from src.khn.models import *


models = [Sys_Menu, Sys_Parameter, Sys_Usr_Actions, User]

if __name__ == "__main__":
    app = create_app()
    if "createdb" in sys.argv:
        with app.app_context():
            for model in models:
                model.create_if_exists()
            print("Database created! ")

    elif "createtb" in sys.argv:
        with app.app_context():
            modulename = sys.argv[2]

            for model in models:
                if modulename == model.__name__:
                    model.create_if_exists()
                    print("Table %s created!" % model.__tablename__)

    elif "seeddb" in sys.argv:
        with app.app_context():
            user = User()
            user.username = 'admin'
            user.first_name = 'admin'
            user.last_name = ''
            user.password = os.environ['APP_ADMIN_PWD']
            user.email = os.environ['APP_ADMIN_EMAIL']

            db.session.add(user)
            db.session.commit()

            admin = Sys_Menu({'title': 'Admin', 'position': 9999})

            db.session.add(admin)
            db.session.commit()

            menu_item = Sys_Menu({'title': 'Menu', 'onclick': "loadApp('khn-menu')", 'parent_id': admin.id})
            db.session.add(menu_item)
            menu_item = Sys_Menu({'title': 'Users', 'onclick': "loadApp('khn-users')", 'parent_id': admin.id})
            db.session.add(menu_item)
            menu_item = Sys_Menu({'title': 'Parameters', 'onclick': "loadApp('khn-parameters')", 'parent_id': admin.id})
            db.session.add(menu_item)
            db.session.commit()

            print("Database seeded! ")
