#!/usr/bin/env bash

#### Clonacion de App que usa modulo KHN
git config --global credential.helper 'cache --timeout 3600'
source venv/bin/activate
sudo rm -r app
git clone --recursive https://gitlab.com/cbrcl/<KHN_APP> app
cd app
git submodule update --init --recursive
cd ..
python app/src/khn/setup/setup.py

