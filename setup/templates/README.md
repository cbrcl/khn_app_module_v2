## Replicacion por primera vez de la aplicación

### Crea entorno de replica de aplicación
    touch app.env && mkdir config tmp
    virtualenv venv
    source venv/bin/activate
    
### Clona aplicación e inicia modulo KHN
    git clone https://gitlab.com/cbrcl/<KHN_APP> app
    cd app
    git submodule update --init --recursive
    cd ..

### Instala requerimientos python de la aplicacion
    pip install -r app/src/khn/setup/requirements.txt

### Editar el archivo app.env con las variables de la instancia
    nano app.env
    
### Crea los archivos de configuracion necesarios para ejecutar la aplicación
    python app/src/khn/setup/setup.py
    
### Crea la base de datos con las tablas de sistema de la aplicacion
    python app/src/khn/setup/create_db.py createdb

<hr>

## Actualizacion de la aplicacion
    git pull
    cd app/src/khn
    git pull