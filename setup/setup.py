################################################################################################
# Este setup crea una serie de archivos que hacen mas facil la replicacion de la aplicacion
# la configuracion de dichos archivos se basa en el archivo app.env
################################################################################################
import os
import stat
import configparser
from os.path import dirname
from os.path import realpath


def set_environ():
    import sys
    reload(sys)
    sys.setdefaultencoding('utf8')

    app_dir = dirname(dirname(dirname(dirname(realpath(__file__)))))
    app_root_dir = dirname(app_dir)

    config = configparser.ConfigParser()
    config.read(os.path.join(app_root_dir, 'app.env'))

    env_vars = {}
    env_vars['PATH_APP'] = app_dir
    env_vars['PATH_ROOT'] = app_root_dir
    env_vars['PATH_UPLOAD'] = os.path.join(env_vars['PATH_ROOT'], 'tmp')
    env_vars['CURRENT_OS_USER'] = env_vars['PATH_ROOT'].split('/')[2]

    for section in config:
        for key in config[section].keys():
            env_vars[key.upper()] = config[section][key]

    for key in env_vars:
        os.environ[key] = env_vars[key]

    return env_vars


def replace_vars(str, vars):
    for key in vars:
        str = str.replace('<%s>' % (key), vars[key])
    return str.strip()


env_vars = set_environ()


def create_custom_file(filename, path=""):
    with open(os.path.join(env_vars['PATH_APP'], "src/khn/setup/templates/", filename), "r") as template:
        if path != "":
            output_filename = os.path.join(env_vars['PATH_ROOT'], path, filename)
        else:
            output_filename = os.path.join(env_vars['PATH_ROOT'], filename)

        with open(output_filename, "w") as output:
            output.write(replace_vars(template.read(), env_vars))
        return output_filename


if __name__ == '__main__':
    env_vars['SERVICE_NAME'] = env_vars['APP_NAME'].lower().replace(' ', '_')

    create_custom_file("uwsgi.ini", "config")
    service_filename = create_custom_file("app.service", "config")
    create_custom_file("db.sql", "config")
    create_custom_file("README.md", "app")
    create_custom_file("run_dev.sh")
    create_custom_file("reinstall.sh")

    os.chmod('run_dev.sh', os.stat('run_dev.sh').st_mode | stat.S_IEXEC)
    os.chmod('reinstall.sh', os.stat('reinstall.sh').st_mode | stat.S_IEXEC)

    print '### Copy service file to service folder'
    print 'sudo cp %s /etc/systemd/system/%s' % (service_filename, "%s.service" % env_vars['SERVICE_NAME'])
    print '### Start service'
    print 'sudo systemctl start %s' % env_vars['SERVICE_NAME']
    print '### Start service on StartUp'
    print 'sudo systemctl enable %s' % env_vars['SERVICE_NAME']
