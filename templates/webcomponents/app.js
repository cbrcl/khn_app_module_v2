KHN_PARAMS = {};
class KHN {
    static get apiUrl(){
        return '/api';
    }

    static getMonths(lang){
        let months={
            ES: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre']
        };
        return months[lang];
    }

    static getShortDaysOfWeek(lang){
        let daysOfWeek={
          ES: ['Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sa', 'Do']
        };
        return daysOfWeek[lang];
    }

    static post(url, data, callbak){
        $.ajax({
            type: 'POST',
            contentType: 'application/json',
            data: JSON.stringify(data),
            dataType: 'json',
            url: url,
            success: function (response) {
                callbak(response);
            }
        });
    }

    static postFormData(url, data, callback){
        $.ajax({
            url: url,
            cache: false,
            contentType: false,
            processData: false,
            data: data,
            type: 'POST',
            success: function(response) {
                callback(response);
            },
            error: function(error) {
                console.log(error);
            }
        });
    }

    static delete(url, callbak){
        $.ajax({
            type: 'DELETE',
            url: url,
            success: function (response) {
                callbak(response);
            }
        });
    }

    static get(url, callback){
        $.get( url, callback);
    }

    static getColor(){
        let color={
            blue: '#007bff',
            red:'#dc3545',
            green: '#28a745',
            yellow: '#ffc107'
        }
    }

    static get bg(){
        return {
            gray: 'bg-secondary',
            green: 'bg-success',
            red: 'bg-danger',
            yellow: 'bg-warning',
            blue: 'bg-primary'
        }
    }

    static get icons() {
        return {
            edit:'fa fa-pencil',
            delete: 'fa fa-trash',
            upload: 'fa fa-upload',
            table: 'fa fa-table',
            hashtag: 'fa fa-hashtag',
            money: 'fa fa-money',
            database:'fa fa-database',
            pdf:'fa fa-file-pdf-o',
            likes: 'fa fa-thumbs-o-up',
            flag: 'fa fa-flag-o',
            star: 'fa fa-star-o',
            files: 'fa fa-files-o',
            dashboard: 'fa fa-tachometer',
            eye: 'fa fa-eye',
            email: 'fa fa-envelope',
            window: 'fa fa-window-maximize'
        }
    }

    static formatValue(value, options){
        let decimals = (options.decimals)? options.decimals:0;

        switch(options.formatter){
            case "int":
                value = (value)?parseFloat(value):0;
                value = value.toFixed(0);
                break;
            case "date":
                let date = (value)?value:'';
                if(date != '')
                    value = KHN.pivotDate(date);
                break;
            case "shortdate":
                let shortdate = (value)?value:'';
                if (shortdate != ''){
                    value = KHN.pivotDate(shortdate);
                    value = value.replace('-20', '-');
                }
                break;
            case "datetime":
                let datetime = (value)?value:'';
                value = KHN.pivotDatetime(datetime);
                break;
            case "decimal":
                value = (value)?parseFloat(value):0;
                value = value.toFixed(decimals);
                break;
            case "money":
                value = (value)?parseFloat(value):0;
                value = new Intl.NumberFormat("es-CL", {minimumFractionDigits: decimals}).format(value);
                value = KHN_PARAMS.CURRENCY_SYMBOL+' '+value;
                break;
            case "html":
                break;
        }

        if(options.suffix)
            value = value + options.suffix;
        if(options.preffix)
            value = options.preffix + value;

        return value
    }

    static pivotDate(value){
        let date = value.substring(0,10).split('-');
        value = date[2]+'-'+date[1]+'-'+date[0];
        return value;
    }

    static pivotDatetime(value){
        if(value){
            let delimiter=value.substring(10,11);
            let time = value.split(delimiter)[1];
            value = KHN.pivotDate(value)+' '+time;
            return value;
        }else{
            return "";
        }
    }

    static dateToString(date){
        let year = date.getFullYear();
        let month = ('0'+(date.getMonth()+1)).slice(-2);
        let day = ('0'+date.getDate()).slice(-2);
        return year+"-"+month+"-"+day;
    }
}

class KPolymer extends Polymer.Element{
    getBg(color){
        return KHN.bg[color];
    }

    getIcon(icon){
        return KHN.icons[icon];
    }

    isSelected(item, selected){
        return item == selected;
    }

    formatDate(value){
        if(value=='' || !value)
            return '';
        return KHN.pivotDate(value);
    }

    formatDatetime(value){
        return KHN.pivotDatetime(value);
    }
}


function toggleSideNav() {
    let sidebar = document.querySelector(".sidebar");
    let main = document.querySelector(".main-wrapper");

    if(sidebar.style.marginLeft=="0px" || sidebar.style.marginLeft==""){
        sidebar.style.marginLeft = "-250px";
        main.style.marginLeft = "0px";
    }else{
        sidebar.style.marginLeft = "0px";
        main.style.marginLeft = "250px";
    }
}

function loadApp(appName){
    let app = document.createElement(appName);
    let main = document.querySelector('#main');
    main.innerHTML='';
    main.appendChild(app);
}

KHN.get(KHN.apiUrl+"/parameter", (response)=> {
    response.items.forEach(item => {
        KHN_PARAMS[item.name] = item.value;
    });

    KHN_PARAMS.currentUser = response.current_user;
});
