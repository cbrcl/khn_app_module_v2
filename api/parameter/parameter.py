import os
from flask import jsonify, request, current_app
from flask_login import current_user
from ...common import KHN_MySql,APIObject, admin_only
from ...models import Sys_Parameter
from ...extensions import db

class parameter(APIObject):
    @staticmethod
    def handle_root():
        response = {
            'status': 'OK',
            'method': request.method
        }
        with KHN_MySql() as mysql:
            query = "SELECT * FROM `sys_parameter` ORDER BY `name`"
            items = mysql.fetch_all(query)
            response['items'] = items
            response['current_user'] = current_user.username
            response['app_name'] = os.environ['APP_NAME']

        return jsonify(response)

    @staticmethod
    @admin_only
    def handle_action(id):
        id = int(id)
        response = {
            'status': 'OK',
            'id': id,
            'method': request.method
        }
        if request.method == 'GET':
            with KHN_MySql() as mysql:
                query = "SELECT * FROM `sys_parameter` WHERE id=%s " % str(id)
                item = mysql.fetch_one(query)
                response['item'] = item

                return jsonify(response)
        elif request.method == 'POST':
            form = request.get_json()

            if id==0:
                form.pop('id', None)
                item = Sys_Parameter()
                item.load_from_dict(form)
                db.session.add(item)
                db.session.commit()
                form['id'] = item.id
            else:
                item = Sys_Parameter.query.get(id)
                item.load_from_dict(form)
                db.session.commit()

            response['item'] = form

            return jsonify(response)
        elif request.method == 'DELETE':
            item = Sys_Parameter.query.get(id)
            db.session.delete(item)
            db.session.commit()
            return jsonify(response)

        return jsonify({})