from flask import jsonify, request, current_app
from ...common import KHN_MySql,APIObject, admin_only
from ...models import User
from ...extensions import db

class user(APIObject):
    @staticmethod
    def handle_root():
        response = {
            'status': 'OK',
            'method': request.method
        }
        with KHN_MySql() as mysql:
            query = "SELECT id, username, first_name, last_name, email, `status` FROM `sys_user` ORDER BY `username`"
            users = mysql.fetch_all(query)
            response['users'] = users

        return jsonify(response)

    @staticmethod
    @admin_only
    def handle_action(id):
        id = int(id)
        response = {
            'status': 'OK',
            'id': id,
            'method': request.method
        }
        if request.method == 'GET':
            with KHN_MySql() as mysql:
                query = "SELECT id, username, first_name, last_name, email, `status` FROM `sys_user` WHERE id=%s " % str(id)
                user = mysql.fetch_one(query)
                response['user'] = user

                return jsonify(response)
        elif request.method == 'POST':
            form = request.get_json()

            if id==0:
                form.pop('id', None)
                user = User()
                user.load_from_dict(form)
                db.session.add(user)
                db.session.commit()
                form['id'] = user.id
            else:
                user = User.query.get(id)
                user.load_from_dict(form)
                db.session.commit()

            response['user'] = form

            return jsonify(response)
        elif request.method == 'DELETE':
            user = User.query.get(id)
            db.session.delete(user)
            db.session.commit()
            return jsonify(response)

        return jsonify({})