from flask import jsonify, request, current_app
from flask_login import current_user
from ...common import KHN_MySql,APIObject, admin_only
from ...models import Sys_Menu
from ...extensions import db


class menu(APIObject):
    @classmethod
    def handle_root(cls):
        response = {
            'status': 'OK',
            'method': request.method
        }
        with KHN_MySql() as mysql:
            query = "SELECT * FROM `sys_menu` WHERE parent_id=0 ORDER BY `position`, title"
            items = mysql.fetch_all(query)

            _items = []
            for item in items:
                if current_user.username == 'admin':
                    query = "SELECT * FROM `sys_menu` WHERE parent_id=%s ORDER BY `position`, title" % item['id']
                else:
                    query = "SELECT * FROM `sys_menu` WHERE parent_id=%s AND access_level='user' ORDER BY `position`, title" % item['id']
                subitems = mysql.fetch_all(query)

                if len(subitems) > 0:
                    item['subitems'] = subitems
                _items.append(item)
            response['items'] = _items

        return jsonify(response)

    @classmethod
    @admin_only
    def handle_action(cls, id):
        id = int(id)
        response = {
            'status': 'OK',
            'id': id,
            'method': request.method
        }
        if request.method == 'GET':
            with KHN_MySql() as mysql:
                query = "SELECT * FROM `sys_menu` WHERE id=%s " % str(id)
                item = mysql.fetch_one(query)

                query = "SELECT * FROM `sys_menu` WHERE parent_id=%s ORDER BY `position`, title" % item['id']
                subitems = mysql.fetch_all(query)
                item['subitems'] = subitems

                response['item'] = item

                return jsonify(response)
        elif request.method == 'POST':
            form = request.get_json()

            if id==0:
                form.pop('id', None)
                item = Sys_Menu()
                item.load_from_dict(form)
                db.session.add(item)
                db.session.commit()
                form['id'] = item.id
            else:
                item = Sys_Menu.query.get(id)
                item.load_from_dict(form)
                db.session.commit()

            response['item'] = form

            return jsonify(response)
        elif request.method == 'DELETE':
            item = Sys_Menu.query.get(id)
            db.session.delete(item)
            db.session.commit()
            return jsonify(response)

        return jsonify(response)