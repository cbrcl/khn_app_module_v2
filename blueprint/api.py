import os
from flask import Blueprint, jsonify, request, current_app
from flask_login import login_required, current_user
from ..common import *
from importlib import import_module

api = Blueprint('api', __name__, template_folder='templates', url_prefix='/api')

custom_api_path = 'src.api.%s.%s'
khn_api_path = 'src.khn.api.%s.%s'


def get_handler(module_name):
    module_path = custom_api_path % (module_name, module_name)
    try:
        handler = getattr(import_module(module_path), module_name)
        return handler
    except ImportError as err:
        #current_app.logger.debug('No custom module named: %s' % module_path)
        module_path = khn_api_path % (module_name, module_name)
        try:
            handler = getattr(import_module(module_path), module_name)
            return handler
        except ImportError as err2:
            #current_app.logger.debug('No custom module named: %s' % module_path)
            pass
    return None


@api.route('/<string:module_name>/', methods=['GET','POST'])
@login_required
def api_handle_root(module_name):
    handler = get_handler(module_name)
    if handler:
        return handler.handle_root()
    return jsonify({'msg': "Module %s doesn't exists" % module_name})


@api.route('/<string:module_name>/<string:id>', methods=['GET', 'POST', 'DELETE'])
@login_required
def api_handle_action(module_name, id):
    handler = get_handler(module_name)
    if handler:
        return handler.handle_action(id)
    return jsonify({'msg': "Module %s doesn't exists" % module_name})


@api.route('/<string:module_name>/<string:id>/<string:subaction>', methods=['GET', 'POST', 'DELETE'])
@login_required
def api_handle_subaction(module_name, id, subaction):
    handler = get_handler(module_name)
    if handler:
        return handler.handle_subaction(id, subaction)
    return jsonify({'msg': "Module %s doesn't exists" % module_name})