from flask import (Blueprint, render_template, request, url_for, redirect, current_app)
from flask_login import login_required, logout_user, login_user, current_user
from ..models import User
from ..common.Utils import get_parameters

auth = Blueprint('auth', __name__)


@auth.route('/login', methods=['GET', 'POST'])
def login():
    if request.method == 'GET':
        template_data = {
            'parameters': get_parameters()
        }
        return render_template('login.html', **template_data)

    user, authenticated = User.authenticate(request.form['username'],
                                            request.form['password'])

    if user:
        if authenticated:
            login_user(user)
            current_user.log_action('login')
            return redirect(url_for('custom_views.home'))

    template_data = {
        'parameters': get_parameters(),
        'error_msg': 'Error de credenciales'
    }
    return render_template('login.html', **template_data)


@auth.route('/logout')
@login_required
def logout():
    current_user.log_action('logout')
    logout_user()
    return redirect(url_for('auth.login'))
