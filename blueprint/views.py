import os
import re
from flask import Blueprint, render_template, current_app, Response
from flask_login import login_required
from ..common import read_file

views = Blueprint('views', __name__, template_folder='templates')


@views.route('/webcomponents/khn/<string:filename>', methods=['GET'])
@login_required
def views_khn_webcomponents(filename):
    path = os.path.join(os.environ['PATH_APP'], 'src', 'khn', 'templates', 'webcomponents', filename)
    content = read_file(path)
    return content


@views.route('/webcomponents/custom/<string:filename>', methods=['GET'])
@login_required
def views_custom_webcomponents(filename):
    path = os.path.join(os.environ['PATH_APP'], 'src', 'templates', 'webcomponents', filename)
    content = read_file(path)
    return content
