## Setting up the Environment /home/<user>/apps/<APP_NAME>
    cd /home/<user>/apps/<APP_NAME>
    touch app.env && mkdir config temp
    virtualenv venv
    source venv/bin/activate

    git clone https://gitlab.com/cbrcl/khn_app_<APP_NAME> app
    cd app
    git submodule update --init --recursive
    cd ..
    pip install -r app/src/khn/setup/requirements.txt

    nano app.env
    #####################################
    [KHN_APP]
        KHN_APP=khn_app_<APP_NAME>

    [APP]
        APP_NAME=<APP_NAME>
        APP_PORT=5000
        APP_ADMIN_PWD=khn
        APP_ADMIN_EMAIL=cristian@albareda.cl

    [MYSQL]
        MYSQL_PORT=3306
        MYSQL_SERVER=localhost
        MYSQL_DATABASE=khn_app
        MYSQL_USER=khn
        MYSQL_PASSWORD=sdfGR$.294
    #####################################

    python app/src/khn/setup/setup.py
    python app/src/khn/setup/create_db.py createdb
