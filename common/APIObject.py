from flask import jsonify, current_app
from KhnBase import KHN_Base
from flask_login import current_user
from flask import request
import datetime
import json

class APIObject(KHN_Base):
    @classmethod
    def handle_root(cls):
        return jsonify({
            'status': 'ERROR',
            'method': request.method,
            'msg': 'Requested API is not implemented'
        })

    @classmethod
    def handle_action(cls, id):
        return jsonify({
            'status': 'ERROR',
            'method': request.method,
            'msg': 'Requested API is not implemented'
        })

    @classmethod
    def log_action(cls, data, title=None):
        if title is None:
            title = "%s: %s" % (request.method, cls.__name__)

        current_user.log_action(title, data)
