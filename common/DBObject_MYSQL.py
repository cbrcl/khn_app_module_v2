import MySQLdb
import os
from flask import current_app


class KHN_MySql(object):
    def __enter__(self):
        self.conn = MySQLdb.connect(user=os.environ['MYSQL_USER'],
                                    host=os.environ['MYSQL_SERVER'],
                                    passwd=os.environ['MYSQL_PASSWORD'],
                                    db=os.environ['MYSQL_DATABASE'])
        self.init_cursor()
        return self

    def init_cursor(self):
        self.cursor = self.conn.cursor(MySQLdb.cursors.DictCursor)
        self.cursor.execute("SET NAMES utf8")

    def query(self, query):
        try:
            self.cursor.execute(query)
        except (MySQLdb.Error, MySQLdb.Warning) as e:
            self.error = e
            current_app.logger.debug(query)
            current_app.logger.debug(e)
            return None

    def fetch_all(self, query):
        self.query(query)
        results = self.cursor.fetchall()

        self.cursor.close()
        self.init_cursor()
        return results

    def get_columns(self):
        columns = [c[0] for c in self.cursor.description]
        return columns

    def fetch_one(self, query):
        results = self.fetch_all(query)
        if len(results) > 0:
            return results[0]
        return None

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.conn.commit()
        self.cursor.close()
        self.conn.close()
