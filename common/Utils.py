import os
import decimal
import json
from datetime import datetime, timedelta, date
from random import randint
from flask import current_app


def json_serial(obj):
    if isinstance(obj, decimal.Decimal):
        return str(obj)
    if isinstance(obj, (datetime, date)):
        return obj.isoformat()

    return None


class MyJSONEncoder(json.JSONEncoder):
    def default(self, obj):
        myobj = json_serial(obj)
        if obj is not None:
            return myobj

        return super(MyJSONEncoder, self).default(obj)

def get_dir(file):
    return os.path.abspath(os.path.dirname(file))

def datetime_handler(x):
    if isinstance(x, datetime):
        return x.isoformat()
    raise TypeError("Unknown type")

def get_current_time():
    return datetime.now()


def get_current_time_plus(days=0, hours=0, minutes=0, seconds=0):
    return get_current_time() + timedelta(days=days, hours=hours, minutes=minutes, seconds=seconds)


def make_dir(dir_path):
    try:
        if not os.path.exists(dir_path):
            os.mkdir(dir_path)
    except Exception, e:
        raise e

def getColors():
    colors = []

    #blue
    colors.append([54, 162, 235])
    # red
    colors.append([255, 99, 132])
    #green
    colors.append([75, 192, 192])
    #darkpink
    colors.append([153, 102, 255])
    # orange
    colors.append([255, 159, 64])
    #yellow
    colors.append([255, 205, 86])
    # gray
    colors.append([201, 203, 207])
    return colors

def getColor(index):
    return getColors()[index]

def getRandomColor():
    index = randint(0, len(getColors())-1)
    color = getColor(index)
    return color

def getRGBAColor(color, alpha=0):
    rgba = 'rgba('+(', '.join(str(c) for c in color))+', '+str(alpha)+')'
    return rgba


def replaceVars(str, vars):
    for k,v in vars:
        str = str.replace('@'+k+'@', v)
    return str

def read_file(filename):
    with open(os.path.join(filename), 'r') as myfile:
        content = myfile.read()
    return content


def get_parameters():
    from DBObject_MYSQL import KHN_MySql

    parameters = {}
    with KHN_MySql() as mysql:
        results = mysql.fetch_all("SELECT * FROM sys_parameter")

        for row in results:
            parameters[row['name']] = row['value']

    return parameters


class SqlWhere(object):
    def __init__(self, operator="AND"):
        self.conditions = {}
        self.operator = " %s " % operator

    def add(self, key, condition):
        self.conditions[key] = condition

    def eval(self, values):
        where = ["1=1"]
        for k, v in self.conditions.items():
            if k in values.keys() and len(values[k].strip()) > 0:
                where.append(v.replace("<"+k+">", values[k].strip() ))

        return "(%s)" % self.operator.join(where)
