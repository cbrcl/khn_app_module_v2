from functools import wraps
from flask_login import current_user
from flask import jsonify


def admin_only(func):
    @wraps(func)
    def func_wrapper(*args, **kwargs):
        if current_user.username == 'admin':
            return func(*args, **kwargs)
        else:
            response = {
                'status': 'ERROR',
                'method': 'Access denied'
            }
            return jsonify(response)
    return func_wrapper
