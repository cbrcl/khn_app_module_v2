import json
import os
import xlrd
from datetime import datetime, date
from flask import current_app
from Utils import datetime_handler, get_dir
from ..extensions import db
from sqlalchemy import exc

class KHN_Base(object):
    def setattr(self, key, value):
        try:
            setattr(self, key, value)
        except:
            pass

    def load_from_object(self, obj):
        for idx, key in enumerate(obj):
            self.setattr(key, obj[key])

    def json(self, columns=[]):
        if len(columns)==0:
            return self.__dict__
        data = {}
        for key in columns:
            value = self.__dict__[key]
            data[key] = value
        return data

    def load_from_dict(self, data):
        for key, value in data.items():
            self.setattr(key, value)

    def get_class_dir(self):
        import inspect
        return get_dir(inspect.getfile(self.__class__))

class KHN_Object(KHN_Base):
    __idname__ = 'id'

    def __init__(self, data = {}):
        self.load_from_dict(data)
        self.created_date = datetime.now()

    def setattr(self, key, value):
        try:
            setattr(self, key, value)
        except:
            pass

    def get_columns(self):
        return self.__table__.columns.keys()

    def load_from_dict(self, data):
        columns = self.get_columns()

        for idx, col in enumerate(data):
            if col in columns:
                self.setattr(col, data[col])

    def __repr__(self):
        columns = self.get_columns()
        data={}
        for col in columns:
            data[col] = getattr(self, col)

        return "<" + type(self).__name__ +"("+ json.dumps(data, default=datetime_handler) + ")>"

    def json(self, columns=None):
        if columns is None:
            columns = self.get_columns()

        data = {}
        for col in columns:
            data[col] = getattr(self, col)
        return data

    def getdefault(self, name):
        default = self.__table__.columns[name].default
        if default:
            return default.arg
        else:
            return None


    @classmethod
    def save(cls, id, data):
        # Add new
        if int(id) == 0:
            data.pop(cls.__idname__, None)
            obj = cls()
            obj.load_from_dict(data)
            db.session.add(obj)
            db.session.commit()
        # Edit existing
        else:
            obj = cls.query.get(id)
            obj.load_from_dict(data)
            db.session.commit()

        return obj

    @classmethod
    def delete(cls, id):
        obj = cls.query.get(id)
        db.session.delete(obj)
        db.session.commit()

    @classmethod
    def create_if_not_exists(cls):
        if not db.engine.dialect.has_table(db.engine, cls.__tablename__):
            db.metadata.drop_all(db.engine, tables=[cls.__table__])
            db.metadata.create_all(db.engine, tables=[cls.__table__])

    @classmethod
    def create_if_exists(cls):
        if db.engine.dialect.has_table(db.engine, cls.__tablename__):
            db.metadata.drop_all(db.engine, tables=[cls.__table__])
        db.metadata.create_all(db.engine, tables=[cls.__table__])

    @classmethod
    def import_from_excel(cls, filename, options={}):
        print("reading file: " + filename)

        # Open the workbook
        xl_workbook = xlrd.open_workbook(filename, encoding_override='utf_8')
        sheet_names = xl_workbook.sheet_names()
        xl_sheet = xl_workbook.sheet_by_name(sheet_names[0])
        headers = xl_sheet.row(0)

        num_rows = xl_sheet.nrows
        for row_idx in range(1, num_rows):
            row = cls.clean_row(xl_sheet.row(row_idx), xl_sheet)

            # create dict
            dict_row = {}
            for col_idx, cell in enumerate(headers):
                key = str(cell.value).lower()
                value = row[col_idx]
                dict_row[key] = value

            new_obj = cls(dict_row)

            #load filename attribute
            new_obj.setattr('src_filename', filename)

            for key, value in options.items():
                new_obj.setattr(key, value)

            #if the created object is valid it is inserted
            if new_obj.is_valid():
                db.session.add(new_obj)

        try:
            db.session.commit()
            return None
        except exc.SQLAlchemyError as e:
            return e


    @classmethod
    def clean_row(cls, row, worksheet, **kwargs):
        """
        Clean cells in a row.
        :param row: Row to clean.
        :param worksheet: Worksheet that row belongs to.
        :param kwargs: May specify trim_nones to True to trim trailing None values.
        """
        result = []
        for cell in row:
            val = cls.clean_cell(cell, worksheet)
            result.append(val)

        return result

    @classmethod
    def clean_cell(cls, cell, worksheet):
        """
        Convert supported xlrd cell types into sensible python types.
        """
        if not isinstance(cell, xlrd.sheet.Cell):
            # Non cells can be passed through, for testing.
            return cell

        if cell.ctype in (xlrd.XL_CELL_EMPTY, xlrd.XL_CELL_BLANK):
            return None
        elif cell.ctype == xlrd.XL_CELL_BOOLEAN:
            return bool(cell.value)
        elif cell.ctype == xlrd.XL_CELL_DATE:
            workbook = worksheet.book
            if not cell:
                return None
            dt_tuple = xlrd.xldate_as_tuple(float(cell.value), workbook.datemode)
            ymd = dt_tuple[:3]
            hms = dt_tuple[3:]
            if ymd == (0, 0, 0):
                # No date, so this is just a time.
                return datetime.time(*hms)

            # We have no way to know if midnight was specified in a datetime
            # or if no time was specified and it was just a date, so default
            # to a datetime:
            return datetime(*dt_tuple)
        elif cell.ctype == xlrd.XL_CELL_NUMBER:
            return float(cell.value)
        elif cell.ctype == xlrd.XL_CELL_TEXT:
            if cell.value=='':
                return None
            return cell.value