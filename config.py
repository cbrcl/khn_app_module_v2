import os
from flask import current_app

class BaseConfig(object):
    PROJECT_ROOT = os.environ["PATH_ROOT"]
    DEBUG = False
    TESTING = False
    # http://flask.pocoo.org/docs/quickstart/#sessions
    SECRET_KEY = ',394$EFDGBXFGSWER$"$"$'

class DefaultConfig(BaseConfig):
    # Statement for enabling the development environment
    SQLALCHEMY_TRACK_MODIFICATIONS = False

    DB_USER = os.environ['MYSQL_USER']
    DB_PASS = os.environ['MYSQL_PASSWORD']
    DB_SERVER = os.environ['MYSQL_SERVER']
    DB_PORT = os.environ['MYSQL_PORT']
    DB_NAME = os.environ['MYSQL_DATABASE']
    SQLALCHEMY_DATABASE_URI = 'mysql://{0}:{1}@{2}:{3}/{4}?charset=utf8'.format(
        DB_USER, DB_PASS, DB_SERVER, DB_PORT, DB_NAME
    )